import nose
from nose.tools import *
from app import app
import unittest


test_app = app.test_client()


def setup():
    # insert test data in db
    pass


def teardown():
    # remove test data from db
    pass


@with_setup(setup, teardown)
def test_homepage():
    response = test_app.get('/')
    eq_(response.status_code, 200)

    html_data = response.data
    assert_equal(html_data, 'Hello world!')

    names = ['joe', 'mary', 'jonathan']
    for name in names:
        response = test_app.get('/greeting/%s' % name)
        eq_(response.status_code, 200)

        html_data = response.data
        eq_(html_data, 'Hello %s!' % name)
