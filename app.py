from flask import Flask

app = Flask(__name__)


@app.route('/')
@app.route('/greeting/<string:username>')
def homepage(username='world'):
    return 'Hello %s!' % username

if __name__ == '__main__':
    app.run()
